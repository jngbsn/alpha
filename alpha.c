#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

bool DEBUG = false;
char* usageText = "Usage: alpha FILE\n";
uint8_t* program;
uint8_t curreg = 0;
uint8_t loopreg = 0;
uint8_t r[10];
uint64_t counter = 0;
uint64_t loopcounter = 0;

// flag and enums
#define TOGGLE(x) FLAGS ^= x
uint8_t FLAGS = 0;
enum {
	F_SET = 1,
	F_PUT = 2,
	F_LOOPINIT = 4,
	F_LOOP = 8,
};

int loadFile(const char* filename)
{
	FILE* f = fopen(filename, "r");
	if(f == NULL)
		return -1;

	int c, i = 1;
	program = malloc(i);
	while((c = fgetc(f)) != EOF) {
		program[i-1] = (uint8_t)c; ++i;
		program = (uint8_t*) realloc(program, i);
	}
	program[i-1] = 0;

	if(DEBUG) printf("%s\n", filename);
	// if(DEBUG) printf("%s\n", program);
	return 0;
}

void interp(char c) {
	switch(c) {
		case 's': {
			TOGGLE(F_SET);
			break;
		}
		case 'p': {
			TOGGLE(F_PUT);
			break;
		}
		case 'o': {
			printf("%c", r[curreg]);
			break;
		}
		case 'i': {
			r[curreg]++;
			break;
		}
		case 'd': {
			r[curreg]--;
			break;
		}
		case 'l': {
			TOGGLE(F_LOOPINIT);
			break;
		}
		default: {
			if(FLAGS & F_SET) {
				curreg = program[counter] - 48;
				if(DEBUG) printf("current reg: %i\n", curreg);
				TOGGLE(F_SET);
			}
			if(FLAGS & F_PUT) {
				char buf[3] = "\0\0\0";
				for(int i = 0; i < 3; ++i) {
					buf[i] = program[counter++];
					if(program[counter] == 'p')
						// counter++;
						break;
				}
				r[curreg] = (uint8_t)atoi(buf);
				if(DEBUG) printf("reg: %i\n", r[curreg]);
				TOGGLE(F_PUT);
			}
		}
	}
}

void step()
{
	// print pointer to current instruction if debug is on, THIS WILL CRASH WITH LOOPS
	// if(DEBUG) {
	// 	char* point = malloc(sizeof(program));
	// 	for(int i = 0; i <= counter; ++i) strcat(point, "^");
	// 	printf("%s\n%s\n", program, point);
	// 	free(point);
	// }
	if(FLAGS & F_LOOPINIT) {
		loopreg = program[counter++] - 48;
		loopcounter = counter--;
		TOGGLE(F_LOOPINIT);
		TOGGLE(F_LOOP);
		if(DEBUG) printf("start of loop at: %i\n", loopcounter);
		if(DEBUG) printf("loopreg: %i\n", loopreg);
	} 
	if(FLAGS & F_LOOP) {
		if(program[counter] == 'l') {
			if(r[loopreg] == 0) {
				counter++;
				TOGGLE(F_LOOP);
				return;
			} else {
				counter = loopcounter;
			}
		}
		interp(program[counter]);
	} else {
		interp(program[counter]);
	}
}

int main(int argc, char* argv[]) 
{
	if(argc < 2) {
		printf(usageText);
		return -1;
	}

	char* filename;
	if(argc > 2) {
		if(strcmp(argv[1], "-d") == 0) {
			DEBUG = true;
		} else {
			printf("unrecognized option, accepted: -d");
			return -1;
		}
		filename = argv[2];
	} else {
		filename = argv[1];
	}

	if(loadFile(filename) < 0) {
		printf("Failed to load file %s\n", filename);
		return -1;
	}

	while(program[counter] != 0) {
		step();
		++counter;
	}

	free(program);
	return 0;
}