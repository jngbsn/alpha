alpha programming language specification - v1.0
----------------------------------------

Only uses a-z, 0-9, and some symbols.

a-z are instructions, 0-9 literals, symbols possibly instructions or syntax.

White space is negligible.

There are 10 registers (0-9), a byte each.


Instructions
------------

    a - 
    b - 
    c - 
    d - decrement current register
    e - 
    f - 
    g - get contents of current register
    h - 
    i - increment current register
    j - 
    k - 
    l - loop contents until next l instruction, if l is followed by a literal, will loop while that register is nonzero, otherwise infinitely
    m - 
    n - 
    o - output current register according to ascii table
    p - put into register, puts literals until next p instruction (eg. p123p would put 123 in current register)
    q - 
    r - 
    s - set current register, params: 0-9
    t - 
    u - 
    v - 
    w - 
    x - 
    y - 
    z - 


Examples
--------

Examples can be found in the 'examples' folder in the repo.